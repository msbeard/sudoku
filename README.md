CS520 Final Project: Sudoku in ATS
======

Developers: Michelle Beard and Abhiram Prasan

Overview
======
For this assignment, we tackled the problem of solving any given 9x9 Sudoku puzzle. 

Rules of Sudoku
======
1. Each row in a grid can only contain the digits 1...9 exactly once.
2. Each column in a grid can only contain the digits 1...9 exactly once.
3. Each 3x3 box in a grid can only contain the digits 1...9 exactly once.

Algorithm
======
There exist many algorithmic techniques to solve Sudoku. The technique we chose was the recursive search algorithm depth-first search (DFS). DFS is a recursive search algorithm which applies operators to nodes without using any special knowledge about the problem domain other than the knowledge about what actions are legal. In general, DFS progressively expands the first child node of the search tree and traverses deeper until it reaches the goal node or reaches a node with no children. Then the search backtracks, returning to the most recent unexplored node, and continues the search from there.

In the case for Sudoku, as the algorithm traverses the tree, at each node a number 1...9 will be chosen to fill a cell. If it violates the rules of the game, the algorithm backtracks up the tree to the most recent node and continues down that path until a goal node is reached. 

Inputs
======
We read in a file which contains a list of partially-filled Sudoku puzzles of varying difficulties. 

Ouputs
======
By choosing a random puzzle from a file, we are able to parse its contents and build a Sudoku board. The actual solving occurs by the method search() that takes a board and starting coordinates to recursively search a board and placing numbers 1...9 in empty slots until the puzzle is solved.

