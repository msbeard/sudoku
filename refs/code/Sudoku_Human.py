from math import sqrt
from time import *
import sys

class Sudoku_Human():
    """ Human Sudoku Class: Solves nxn Sudoku puzzles using singles,
        lone rangers, twins, and triplets.
        Will solve most easy and medium Sudoku puzzles.
        Every row, column, and sub-grid contains the digits 1...n once.
    """
    def __init__(self, n, grid):
        self.n = n
        self.grid = grid

    def draw_sudoku(self):
        for r in range(self.n):
            for c in range(self.n):
                print str(self.grid[r][c]) + "",
                if c % self.n == self.n - 1:
                    print "\n"

    def CRME(self, row, col):
        shift = int(sqrt(self.n))
        mrow = row - (row % shift)
        mcol = col - (col % shift)
        possible = range(1, self.n+1)
        
        # Scan Column
        for c in range(self.n):
            if self.grid[row][c] in possible:
                possible.remove(self.grid[row][c])
        
        # Scan Row
        for r in range(self.n):
            if self.grid[r][col] in possible:
                possible.remove(self.grid[r][col])

        # Scan Minigrid
        for rr in range(mrow, mrow+shift):
            for cc in range(mcol, mcol+shift):
                if self.grid[rr][cc] in possible:
                    possible.remove(self.grid[rr][cc])

        return possible

    def NakedSingle(self):
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] == 0:
                    poss = self.CRME(r, c)
                    if len(poss) == 1:
                        #print "Solved using Singles at grid[%d][%d] = %d", (r+1, c+1, poss[0])
                        self.grid[r][c] = poss[0]
    
    def HiddenSingleInMinigrids(self):
        shift = int(sqrt(self.n))
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] == 0:
                    poss = self.CRME(r, c)
                    # scan minigrid for hidden singles
                    mrow = r - (r % shift)
                    mcol = c - (c % shift)
                    for rr in range(mrow, mrow+shift):
                        for cc in range(mcol, mcol+shift):
                            m = []
                            if self.grid[rr][cc] == 0:
                                val = self.CRME(rr, cc)
                                for i in poss:
                                    if (i not in val) and (i not in m):
                                        m.append(int(i))
                    if len(m) == 1:
                        print "Solved using Hidden Singles in Minigrids at grid[%d][%d] = %d", (r+1, c+1, m[0])
                        self.grid[r][c] = m.pop()

    def LoneRangersInMinigrids(self):
        shift = int(sqrt(self.n))
        for v in range(1, self.n+1):
            for r in range(0, self.n, shift):
                for c in range(0, self.n, shift):
                    # Initialize frequency counter
                    count = 0
                    for rr in range(0, shift):
                        for cc in range(0, shift):
                            if self.grid[rr+r][c+cc] == 0 and v in self.CRME(rr+r, c+cc):
                                count += 1
                                rpos, cpos = rr + r, cc + c
                    if count == 1:
                        #print "Solved using Lone Rangers in Minigrids"
                        self.grid[rpos][cpos] = v
                    
    def LoneRangersInRows(self):
        for r in range(self.n):
            for v in range(1, self.n+1):
                # Initialize frequency counter
                count = 0
                for c in range(self.n):
                    if self.grid[r][c] == 0 and v in self.CRME(r, c):
                        count += 1
                        rpos, cpos = r, c
                if count == 1:
                    #print "Solved using Lone Rangers in Rows at grid[%d][%d] = %d" % (rpos+1, cpos+1, v)
                    self.grid[rpos][cpos] = v                

    def LoneRangersInCols(self):
        for c in range(self.n):
            for v in range(1, self.n+1):
                #Initialize frequency counter
                count = 0
                for r in range(self.n):
                   if self.grid[r][c] == 0 and v in self.CRME(r, c):
                       count += 1
                       rpos, cpos = r, c
                if count == 1:
                    #print "Solved using Lone Rangers in Cols at grid[%d][%d] = %d" % (rpos+1, cpos+1, v)
                    self.grid[rpos][cpos]= v
                        
    # Naked Twins
    def TwinsInRows(self):
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] == 0 and len(self.CRME(r, c)) == 2:
                    # possible twin
                    poss_twin = self.CRME(r, c)

                    # scan columns in this row for second pair
                    for cc in range(c+1, self.n):
                        if poss_twin == self.CRME(r, cc) and self.grid[r][cc] == 0:
                            #twins found
                            twin = self.CRME(r, cc)
                            
                            # rescan entire row and remove twins from possible5 values
                            for ccc in range(self.n):
                                if self.grid[r][ccc] == 0 and ccc != c and ccc != cc:
                                    possible = self.CRME(r, ccc)
                                    
                                    #remove first twin from possible#
                                    if twin[0] in possible:
                                        possible.remove(twin[0])
                                        
                                    #remove second twin from possible#
                                    if twin[1] in possible:
                                        possible.remove(twin[1])

                                    if len(possible) == 1:
                                        #print "Solved using Naked Twins in Rows at grid[%d][%d] = %d" % (r+1, ccc+1, possible[0])
                                        self.grid[r][ccc] = possible[0]
                                        
    def TwinsInCols(self):
        for c in range(self.n):
            for r in range(self.n):
                if self.grid[r][c] == 0 and len(self.CRME(r, c)) == 2:
                    # possible twin
                    poss_twin = self.CRME(r, c)

                    #scan rows in this column for second pair
                    for rr in range(r+1, self.n):
                        if poss_twin == self.CRME(rr, c) and self.grid[rr][c] == 0:
                            # twin found
                            twin = self.CRME(rr, c)
                            
                            # rescan entire column and remove twins from possible values
                            for rrr in range(self.n):
                                if self.grid[rrr][c] == 0 and rrr != r and rrr != rr:
                                    possible = self.CRME(rrr, c)
                                    
                                    #remove first twin from possible#
                                    if twin[0] in possible:
                                        possible.remove(twin[0])
                                        
                                    #remove second twin from possible#
                                    if twin[1] in possible:
                                        possible.remove(twin[1])

                                    if len(possible) == 1:
                                        #print "Solved using Naked Twins in Cols at grid[%d][%d] = %d" % (rrr+1, c+1, possible[0])
                                        self.grid[rrr][c] = possible[0]

    def TwinsInMinigrids(self):
        shift = int(sqrt(self.n))
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] == 0 and len(self.CRME(r, c)) == 2:
                    # possible twin
                    poss_twin = self.CRME(r, c)

                    # scan minigrid for other twin
                    mrow = r - (r % shift)
                    mcol = c - (c % shift)
                    for rr in range(mrow, mrow+shift):
                        for cc in range(mcol, mcol+shift):
                            if rr != r and cc != c and self.grid[rr][cc] == 0 and self.CRME(rr, cc) == poss_twin:
                                # found other twin
                                twin = self.CRME(rr, cc)
                                #rsub = rr - (rr % shift)
                                #csub = cc - (cc % shift)
                                # rescan minigrid and remove twins from possible values
                                for rrr in range(mrow, mrow+shift):
                                    for ccc in range(mcol, mcol+shift):
                                        if self.grid[rrr][ccc] == 0 and rrr != r and rrr != rr and ccc != c and ccc != cc:
                                            possible = self.CRME(rrr, ccc)

                                            # remove first twin from possible
                                            if twin[0] in possible:
                                                possible.remove(twin[0])

                                            # remove second twin from possible
                                            if twin[1] in possible:
                                                possible.remove(twin[1])

                                            if len(possible) == 1:
                                                #print "Solved using Naked Twins in Minigrids"
                                                self.grid[rrr][ccc] = possible.pop()

    def TripletsInRows(self):
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] == 0 and len(self.CRME(r, c)) == 3:
                    # possible triplet
                    poss_trip = self.CRME(r, c)

                    # scan columns in this row for second triplet
                    for cc in range(c+1, self.n):
                        if poss_trip == self.CRME(r, cc) and self.grid[r][cc] == 0:
                            #second triplet found
                            poss_trip2 = self.CRME(r, cc)
                            for ccc in range(cc+1, self.n):
                                if poss_trip2 == self.CRME(r, ccc) and self.grid[r][ccc] == 0:
                                    #third triplet found
                                    trip = self.CRME(r, ccc)
                                    # rescan entire row and remove triplets from possible5 values
                                    for cccc in range(self.n):
                                        if self.grid[r][cccc] == 0 and cccc != c and cccc != cc and cccc != ccc:
                                            possible = self.CRME(r, cccc)
                                            
                                            #remove first trip from possible#
                                            if trip[0] in possible:
                                                possible.remove(trip[0])
                                                
                                            #remove second trip from possible#
                                            if trip[1] in possible:
                                                possible.remove(trip[1])

                                            #remove last trip from possible#
                                            if trip[2] in possible:
                                                possible.remove(trip[2])

                                            if len(possible) == 1:
                                                #print "Solved using Triplets in Rows at grid[%d][%d] = %d" % (r+1, cccc+1, possible[0])
                                                self.grid[r][cccc] = possible[0]

    def TripletsInCols(self):
        for c in range(self.n):
            for r in range(self.n):
                if self.grid[r][c] == 0 and len(self.CRME(r, c)) == 3:
                    # possible triplet
                    poss_trip = self.CRME(r, c)

                    #scan rows in this column for second triplet
                    for rr in range(r+1, self.n):
                        if poss_trip == self.CRME(rr, c) and self.grid[rr][c] == 0:
                            # second triplet found
                            poss_trip2 = self.CRME(rr, c)

                            for rrr in range(rr+1, self.n):
                                if poss_trip2 == self.CRME(rrr, c) and self.grid[rrr][c] == 0:
                                    # last triplet found
                                    trip = self.CRME(rrr, c)
                                    
                                    # rescan entire column and remove triplets from possible values
                                    for rrrr in range(self.n):
                                        if self.grid[rrrr][c] == 0 and rrrr != r and rrrr != rr and rrrr != rrr:
                                            possible = self.CRME(rrrr, c)
                                            
                                            #remove first twin from possible#
                                            if trip[0] in possible:
                                                possible.remove(trip[0])
                                                
                                            #remove second twin from possible#
                                            if trip[1] in possible:
                                                possible.remove(trip[1])

                                            #remove last triplet from possible#
                                            if trip[2] in possible:
                                                possible.remove(trip[2])
                                                
                                            if len(possible) == 1:
                                                #print "Solved using Triples in Cols at grid[%d][%d] = %d" % (rrrr+1, c+1, possible[0])
                                                self.grid[rrrr][c] = possible[0]

    def TripletsInMinigrids(self):
        shift = int(sqrt(self.n))
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] == 0 and len(self.CRME(r, c)) == 3:
                    # possible triplet
                    poss_trip = self.CRME(r, c)

                    # scan minigrid for other triplet
                    mrow = r - (r % shift)
                    mcol = c - (c % shift)
                    for rr in range(mrow, mrow+shift):
                        for cc in range(mcol, mcol+shift):
                            if rr != r and cc != c and self.grid[rr][cc] == 0 and self.CRME(rr, cc) == poss_trip:
                                # found second triplet
                                poss_trip2 = self.CRME(rr, cc)
                                #rsub = rr - (rr % shift)
                                #csub = cc - (cc % shift)
                                # scan minigrind again and find third triplet
                                for rrr in range(mrow, mrow+shift):
                                    for ccc in range(mcol, mcol+shift):
                                        if rrr != rr and rrr != r and ccc != cc and ccc != cc and self.grid[rrr][ccc] == 0 and self.CRME(rrr, ccc) == poss_trip2:
                                            #found third triplet in minigrid
                                            trip = self.CRME(rrr, ccc)
                                            #recan entire minigrid and remove triplet values from list of possible values in other cells
                                            for rrrr in range(mrow, mrow+shift):
                                                for cccc in range(mcol, mcol+shift):
                                                    if self.grid[rrrr][cccc] == 0 and rrrr != r and rrrr != rr and rrrr != rrr and cccc != c and cccc != cc and cccc != ccc:
                                                        possible = self.CRME(rrrr, cccc)

                                                        # remove first triplet from possible
                                                        if trip[0] in possible:
                                                            possible.remove(trip[0])

                                                        # remove second triplet from possible
                                                        if trip[1] in possible:
                                                            possible.remove(trip[1])

                                                        # remove last triplet from possible
                                                        if trip[2] in possible:
                                                            possible.remove(trip[2])

                                                        if len(possible) == 1:
                                                            #print "Solved using Triplets in Minigrids"
                                                            self.grid[rrrr][cccc] = possible[0]                     
    
    def isPuzzleSolved(self):
        count = 0
        for r in range(self.n):
            for c in range(self.n):
                if self.grid[r][c] != 0:
                    count += 1

        if count == self.n*self.n:
            return False
        return True
           
    def HumanSolve(self):
        self.NakedSingle()
#        self.HiddenSingleInMinigrids()
        self.LoneRangersInMinigrids()
        self.LoneRangersInRows()
        self.LoneRangersInCols()
        self.TwinsInMinigrids()
        self.TwinsInRows()
        self.TwinsInCols()
        self.TripletsInMinigrids()
        self.TripletsInRows()
        self.TripletsInCols()
        
def human_solver():
    time_counter = 0
    solved_counter = 0
    total_counter = 0
    if len(sys.argv) > 1:
        f = open(sys.argv[1])
        for line in f:
            s = line
            board = [[ 0 for row in range(9) ] for col in range(9) ]
            for i in range(len(s) - 2):
                board[int(i/9)][i%9] = int(s[i])          
            n = len(board)
            h = Sudoku_Human(n, board)
            #h.draw_sudoku()
            start = time()
        
            while h.isPuzzleSolved():
                h.HumanSolve()        
                elapsed = time() - start
                if elapsed > 1:
                   # print "No Solution Found"
                    time_counter += elapsed
                    total_counter += 1
                    break
                elif h.isPuzzleSolved() != True:
                    print "Solution found in %.4f seconds\n" % elapsed
                    #h.draw_sudoku()
                    time_counter += elapsed
                    total_counter += 1
                    solved_counter += 1
                    break                        
        f.close()
        print "Total time = %.4f" % time_counter
        print "Solved %d/%d" % (solved_counter, total_counter)
            
#human_solver()
