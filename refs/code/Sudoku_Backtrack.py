#!/usr/bin/python
from __future__ import division
from math import sqrt
from time import *

import sys

class Sudoku_Backtrack():
    """ Brute force Sudoku solver. Can solve any solvable Sudoku puzzle.
        Every row, column, and sub-grid contains the digits 1...n once.
     """
    def __init__(self, n, grid, children, parent):
        self.n = n
        self.grid = grid
        self.children = children
        self.parent = parent

    def DFS(self, row, col):
        if col == self.n:
            col = 0
            row += 1
        if row == self.n and col == 0: # reached end of tree
            return True
        if self.grid[row][col] > 0: # examining parent/child node
            self.children += 1
            self.parent += 1
            return self.DFS(row, col+1)
        for v in range(1, self.n+1):
            if not self.check(row, col, v): # value does not break rules
                self.children += 1               # created child node  
                self.grid[row][col] = v     # assign value to node
                if self.DFS(row, col+1):    # proceed to next node
                    return True
        # backtrack/and erase value at node
        self.grid[row][col] = 0
        return False
   
    def checkRow(self, row, val):
        for c in range(self.n):
            if val == self.grid[row][c]:
                return True
        return False

    def checkCol(self, col, val):
        for r in range(self.n):
            if val == self.grid[r][col]:
                return True
        return False

    def checkSquare(self, row, col, val):
        shift = int(sqrt(self.n))
        mrow = row - (row % shift)
        mcol = col - (col % shift)
        for rr in range (mrow, mrow+shift):
            for cc in range(mcol, mcol+shift):
                if val == self.grid[rr][cc]:
                    return True
        return False

    def check(self, row, col, val):
        if self.checkRow(row, val):
            return True
        if self.checkCol(col, val):
            return True
        if self.checkSquare(row, col, val):
            return True
        return False  

def play():
    time_counter = 0
    counter = 0
    tot_children = 0
    tot_parent = 0
    if sys.argv > 1:
        f = open(sys.argv[1])       
        for line in f:
            s = line
            board = [[ 0 for row in range(9) ] for col in range(9) ]
            for i in range(len(s) - 2):
                board[int(i/9)][i%9] = int(s[i])
            n = len(board)
            b = Sudoku_Backtrack(n, board, 0, 0)
            start = time()
            b.DFS(0, 0)
            elapsed = time() - start
            time_counter += elapsed
            counter += 1
            print counter
            tot_children += b.children
            tot_parent += b.parent
            print "Solution found in %.4f seconds" % (time_counter)
        avg_children = tot_children/counter
        avg_parent = tot_parent/counter
        print "Children = ", avg_children
        print "Parent = ", avg_parent
        print "Branching factor = ", avg_children/avg_parent
        f.close()         
#play()
