from __future__ import division
import sys
from time import *
from Sudoku_Backtrack import *
from Sudoku_Human import *

class HybridSudoku(Sudoku_Backtrack, Sudoku_Human):
    """ Hybrid Sudoku Solver Class: Solves nxn Sudoku puzzles by using
        human logic techniques. If no solution can be found using human
        logic, resort to brute force algorithm to complete puzzle.
    """

    def __init__(self, n, grid, children, parent):
        self.n = n
        self.grid = grid
        self.children = children
        self.parent = parent


def hybrid_solver():
    count = 0
    time_counter = 0
    tot_children = 0
    tot_parent = 0
    if len(sys.argv[1]) > 1:
        f = open(sys.argv[1])
        for line in f:
            s = line
            board = [[ 0 for row in range(9) ] for col in range(9) ]
            for i in range(len(s) - 2):
                board[int(i/9)][i%9] = int(s[i])
            n = len(board)
            h = HybridSudoku(n, board, 0, 0)
            start = time()

            while h.isPuzzleSolved():
                h.HumanSolve()
                elapsed = time() - start
                if elapsed > 0.5:
                    h.DFS(0,0)
                    tot_children +=  h.children
                    tot_parent +=  h.parent
                    break
            count += 1
            time_counter += time() - start
        print "Solutions found in %.4f seconds\n" % time_counter
        avg_children = tot_children/count
        avg_parent = tot_parent/count
        print "Children = ", avg_children
        print "Parents = ", avg_parent
        print "Branching Factor = ", avg_children/avg_parent
        f.close()

hybrid_solver()
