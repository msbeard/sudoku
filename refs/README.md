I have provided some reference material to get us familar with the logic game
Sudoku. 

code: Previous code I wrote back in college to solve Sudoku puzzles. I also 
provided Peter Norvig's solution (which is pretty interesting so take a look at it). 

test: An example Sudoku game.

Thesis.pdf: My thesis was on the algorithmic study of Sudoku solvers (not generators).
