(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)

staload "./sudoku.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"
staload "libc/SATS/math.sats"
staload _ = "libats/ML/DATS/array0.dats"

(* ****** ****** *)

// Check if element v is all ready in the current row
implement
check_row (B, r, v) = 
let
  val n = g0uint2int_size_int (B.size)
  val xs = B[r]
  fun loop (i: int) : bool = 
  if i < n then 
    if xs[i] = v then false
    else loop (i+1)
  else true 
in
  loop (0)
end // end of [check_row]

(* ****** ****** *)

// Check if element v exists in the current column
implement 
check_col (B, c, v) = 
let
  val n = g0uint2int_size_int (B.size)
  fun loop (i: int) : bool = 
  if i < n then 
    let
      val xs = B[i]  // current row
      val cv = xs[c] // jump to column entry
    in
      if cv = v then false
      else loop (i+1)
    end 
  else true
in
  loop (0)
end // end of [check_col]

(* ****** ****** *)

// Sweep across columns in square
extern fun 
check_square (B: board, r: int, c: int, s: int, v: int) : bool

implement 
check_square (B, r, c, s, v) = 
let
  fun loop (i: int) : bool = 
  if i < c+s then 
    let
      val xs = B[r]
      val cv = xs[i]
    in
      if cv = v then false 
      else loop (i+1)
    end // end of [loop]
  else true
in
  loop (c)
end // end of [check_box]

// Check if element v exists in the current box
implement 
check_box (B, r, c, v) =
let
  val n = g0uint2int_size_int (B.size)
  val shift = 3
  val mrow = r - (r mod shift)
  val mcol = c - (c mod shift)
  fun loop (i: int) : bool = 
  if i < mrow+shift then 
      if check_square (B, i, mcol, shift, v) then loop (i+1)
      else false
  else true // end of [loop]
in
  loop (mrow)
end // end of [check_box]

(* ****** ****** *)

implement check (B, r, c, v) =
  check_row (B, r, v) andalso
  check_col (B, c, v) andalso
  check_box (B, r, c, v)
// end of [check]

(* ****** ****** *)

(* end of [sudoku_check.dats] *)
