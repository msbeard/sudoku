(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)

staload "./sudoku.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"
staload _ = "libats/ML/DATS/array0.dats"

(* ****** ****** *)

dynload "./sudoku.sats"
dynload "./sudoku_board.dats"
dynload "./sudoku_print.dats"
dynload "./sudoku_check.dats"
dynload "./sudoku_search.dats"
dynload "./sudoku_import.dats"

(* ****** ****** *)

val () = println! ("Greetings from sudoku!")

(* ****** ****** *)

dynload "./test_sudoku.dats"

(* ****** ****** *)

implement main0 () = ()

(* ****** ****** *)

(* end of [sudoku.dats] *)
