(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)

staload "./sudoku.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"
staload _ = "libats/ML/DATS/array0.dats"

(* ****** ****** *)

implement
print_board (B) = fprint_board (stdout_ref, B)

(* ****** ****** *)

implement 
fprint_board (out, B) = 
let
val n = g0uint2int_size_int (B.size)
val () = assertloc (n = 9)
fun loop (i: int) : void = 
  if i < n then
   let  
    val xs = B[i]
    val () = array0_foreach<int> (xs, lam (x) => (print_int (x); print_char ('|')))
    val () = print_newline ()
   in
     loop (i+1)
   end
  else () 
in
loop (0)
end // end of [print_board]

(* ****** ****** *)

(* end of [sudoku_print.dats] *)
