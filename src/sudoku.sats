(* ****** ****** *)
//
// Sudoku
//
// Michelle Beard <msbeard@bu.edu>
// Abhiram Prasam <abhiram@bu.edu>
//
(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"

(* ****** ****** *)

typedef board = array0 (array0 (int))

(* ****** ****** *)

// Sudoku board operations
fun get_entry : (board, int, int) -> int
fun set_entry : (board, int, int, int) -> void
fun reset_entry : (board, int, int) -> void

// Test Conditions
fun check_row : (board, int, int) -> bool
fun check_col : (board, int, int) -> bool
fun check_box : (board, int, int, int) -> bool
fun check : (board, int, int, int) -> bool

// DFS + Backtracking
fun search : (board, int, int) -> bool

// Print Sudoku board
fun print_board : (board) -> void
overload print with print_board 
fun fprint_board : (FILEref, board) -> void
overload fprint with fprint_board
 
// Import Sudoku puzzle from stdin
fun create_puzzle : (string) -> board

(* ****** ****** *)

(* end of [sudoku.sats] *)
