(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)

staload "./sudoku.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"
staload _ = "libats/ML/DATS/array0.dats"

(* ****** ****** *)

// Given a partially filled in board, attempt to assign 
// values to all unassigned locations in such a way to 
// meet the requirements for a Sudoku solution
// (i.e. no duplicates across rows, columns, and boxes)
implement search (B, r, c) = 
let
  val N = g0uint2int_size_int (B.size)
in
  if c = N then search (B, r+1, 0)
  else if r = N then true // Solution found!
  else if get_entry (B, r, c) > 0 then search (B, r, c+1)
  else 
    let fun loop (v: int) : bool = 
      if v <= N then 
        if check (B, r, c, v) then 
	  let
	    val () = set_entry (B, r, c, v)
	  in
	    if search (B, r, c+1) then true
	    else let val () = reset_entry (B, r, c) in loop (v+1) end
	  end
        else loop (v+1)
      else false
    in
      loop (1)
    end
end // end of [search] 

(* ****** ****** *)

(* end of [sudoku_search.dats] *)
