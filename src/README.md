Code overview:

Makefile : 		Compile and link Sudoku source code

sudoku.sats: 		Sudoku definitions 

sudoku.dats:	 	Dynamically load Sudoku libraries

sudoku_check.dats: 	Checks to determine if an entry is valid

sudoku_import.dats:	Import a Sudoku puzzle from a file

sudoku_print.dats:	Print a Sudoku puzzle to standard output

sudoku_search.dats:	Sudoku puzzle solver (depth first search with backtracking)

test_sudoku.dats:	Example Sudoku puzzle and generated solution

Michelle Beard
