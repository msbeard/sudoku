(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)

staload "./sudoku.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"
staload _ = "libats/ML/DATS/array0.dats"

(* ****** ****** *)

implement 
get_entry (B, r, c) =
let
  val xs = B[r]
in
  xs[c]
end // end of [get_entry]

implement
set_entry (B, r, c, v) =
let
  val xs = B[r]
  val () = xs[c] := v
  val () = B[r] := xs
in
  // nothing
end // end of [set_entry]

implement
reset_entry (B, r, c) = 
set_entry (B, r, c, 0)

(* ****** ****** *)

(* end of [sudoku_board.dats] *)

