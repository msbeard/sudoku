(* ****** ****** *)

#include "share/atspre_staload.hats"
#include "share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload "./sudoku.sats"
staload TIME = "libc/SATS/time.sats"
staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)
//function to read all the puzzles from the file and store in an array
extern fun
array0_make_fileref (FILEref) : array0 (char)

implement
array0_make_fileref (inp) = A0 where
{
  val cs = fileref_get_file_charlst (inp)
  val A0 = array0_make_list ($UN.castvwtp1{list0(char)}(cs))
  val ((*freed*)) = list_vt_free (cs)
}

(* ****** ****** *)
//function to pick a random puzzle from the provided list
extern fun 
arr_gen (puzz: array0 (char), ran: int) : array0 (int)

implement
arr_gen (puzz, ran) =
array0_tabulate<int> (
  i2sz(81), lam (i) => puzz[$UN.cast2size(ran*82)+i]-'0'
)

(* ****** ****** *)
//function to create the 9X9 board
extern
fun board_gen (A: array0(int)): array0(array0(int))

implement
board_gen (A) =
array0_tabulate<array0(int)> (
  i2sz(9), lam (i) => array0_make_subarray (A, i2sz(9)*i, i2sz(9))
)

(* ****** ****** *)

implement 
create_puzzle (str) = 
let
  val inp = fileref_open_exn (str, file_mode_r)
  val res = array0_make_fileref(inp)
  val ((*void*)) = fileref_close (inp)
  val ran = $UN.cast2int($TIME.time_get()) mod 244
  val choice = arr_gen(res,ran)
  val puzzle = board_gen(choice)
in
  puzzle
end // end of [create_puzzle]

(* ****** ****** *)

(* end of [sudoku_import.dats] *)

