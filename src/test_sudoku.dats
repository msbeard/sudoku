(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)

staload "./sudoku.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/array0.sats"
staload _ = "libats/ML/DATS/array0.dats"

(* ****** ****** *)

extern val puzzle : array0 (array0 (int))
extern val solution : array0 (array0 (int))

(* ****** ****** *)

local

val p1 = (array0)$arrpsz{int}(7, 0, 0, 0, 0, 6, 0, 0, 5) 
val p2 = (array0)$arrpsz{int}(1, 3, 0, 7, 0, 0, 4, 0, 0)
val p3 = (array0)$arrpsz{int}(5, 0, 0, 0, 0, 2, 3, 7, 0)
val p4 = (array0)$arrpsz{int}(0, 0, 0, 0, 0, 0, 0, 4, 0)
val p5 = (array0)$arrpsz{int}(0, 0, 0, 0, 0, 0, 0, 0, 6)
val p6 = (array0)$arrpsz{int}(0, 0, 0, 0, 0, 4, 0, 3, 2)
val p7 = (array0)$arrpsz{int}(0, 6, 0, 1, 9, 0, 2, 0, 0)
val p8 = (array0)$arrpsz{int}(0, 0, 5, 0, 0, 0, 6, 0, 1)
val p9 = (array0)$arrpsz{int}(9, 0, 0, 0, 3, 0, 0, 5, 0)

in

implement 
puzzle = (array0)$arrpsz{array0(int)}(p1, p2, p3, p4, p5, p6, p7, p8, p9)

end // end of [local] 

(* ****** ****** *)

val () = println! (puzzle)

// Lets play Sudoku!
val res = search (puzzle, 0, 0)
val () = println! ("Sudoku puzzle solved!")
val () = println! (puzzle)

// Play another
val game = create_puzzle ("puzzle_list.txt")
val () = println! (game)
val res = search (game, 0, 0)
val () = println! (game)

(* ****** ****** *)

(* end of [test_sudoku.dats] *)
